import datetime

from bluepy.bluepy.btle import *
import os
import xml.etree.ElementTree
import psycopg2
import time
import threading
import logging

conf_file_name = "/home/wimoto/work/config.xml"   # configuration file name

debug = 0


def get_param_from_xml(param):
    """
    Get configuration parameters from the config.xml
    :param param: parameter name
    :return: if not exists, return None
    """
    root = xml.etree.ElementTree.parse(conf_file_name).getroot()
    tmp = None
    for child_of_root in root:
        if child_of_root.tag == param:
            tmp = child_of_root.text
            break
    return tmp

def get_dev_name_list():
    """
    get allowed device name list from the config file
    :return: list of device names
    """
    root = xml.etree.ElementTree.parse(conf_file_name).getroot()

    list_tag = None
    for tag in root:
        if tag.tag == "DEVICE_NAME_LIST":
            list_tag = tag
            break

    name_list = []
    for tag in list_tag:
        name_list.append(tag.text)

    return name_list


class ScanDelegate(DefaultDelegate):
    """
        Base class for BLE scanner
    """
    def __init__(self):
        DefaultDelegate.__init__(self)


class Securimote(threading.Thread):
    """
        Receive data from the device after scanned
    """
    conn = None
    bConnected = False
    dev_name = ''
    address = ''
    rssi = ''
    _stop_event = threading.Event()

    def __init__(self, dev):
        """
        Constructor of class base on thread

        :param dev: tuple which contains device information. type : (device_name, mac_address, rssi)
        :return:
        """
        threading.Thread.__init__(self)  # constructor of parent class
        self._stop = threading.Event()
        self.dev_name = dev[0]
        self.address = dev[1]
        self.rssi = dev[2]
        self.conn = Peripheral(self.address, "random")    # address type of Securimote is 'random'
        self.bConnected = self.conn.bConnected
        if not self.bConnected:
            print "Failed to connect to ", self.address

    def run(self):
        """
            Main function when thread starts
        :return:
        """
        self._stop_event.clear()

        if self.bConnected:
            logging.info("Started receive.")

            while not self._stop_event.isSet():
                try:
                    s_time = time.time()

                    # get PIR sensor data's UUID
                    uuid = get_param_from_xml("UUID")

                    svc = self.conn.getServiceByUUID(uuid)

                    for ch in svc.getCharacteristics():
                        try:
                            if hex(ch.handle) == "0x19":     # Handle of PIR value is 0x19
                                val = ch.read().strip()     # remove spaces
                                if val[0] != '\x00':
                                    self.upload_db((self.dev_name, self.address, self.rssi))
                                else:
                                    if debug > 0:
                                        print "No movement is detected on ", self.dev_name
                        except BTLEException as e:
                            print("    ->", e)
                    t_delta = time.time() - s_time
                    if debug > 0:
                        print "Elaped time of %s : %s" % (self.dev_name, t_delta)

                    if t_delta < 1.5:
                        time.sleep(1.5 - t_delta)

                except:
                    pass

    def upload_db(self, my_dev):
        """
        Upload detected data to postgre db
        Host name, DB name, user name, password are gathered from the configuration file
        :param my_dev: tuple contains device information. type : (device_name, mac_address, rssi)
        :return:
        """
        print "Uploading data."
        print "Device name : %s, value : %s, Strength : %s" % my_dev

        # get parameters from the configuration file
        host_name = get_param_from_xml('HOST_NAME')
        db_name = get_param_from_xml('DB_NAME')
        username = get_param_from_xml('USER_NAME')
        password = get_param_from_xml('PASSWORD')

        # trying to connect to DB
        conn = None
        try:
            uri = "host=" + host_name + " dbname=" + db_name + " user=" + username + " password=" + password
            conn = psycopg2.connect(uri)
        except Exception as e:
            print "Failed to connect to database"
            logging.error("Failed to connect to database, %s" % e)
            return False

        cur = conn.cursor()
        try:
            # Create table if not exists
            query = "CREATE TABLE IF NOT EXISTS securimote (datetime varchar(30) NOT NULL, " \
                    "device_name varchar(30) NOT NULL, rssi integer NOT NULL, PRIMARY KEY (datetime));"
            cur.execute(query)
            conn.commit()

            # get date & time
            str_time = datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S")

            # upload data to db
            query = "INSERT INTO securimote (datetime, device_name, rssi) VALUES " \
                    "('" + str_time + "', '" + my_dev[0] + "', " + str(my_dev[2]) + ")"

            cur.execute(query)
            conn.commit()

        except psycopg2.DatabaseError, e:
            print "Error...", e
            logging.error("DB error: %s" % e)
            if conn:
                conn.rollback()
        except Exception as e:
            print e
            logging.error("DB error : %s" % e)
        finally:
            if conn:
                conn.rollback()

    def get_device_name(self):
        return self.dev_name

    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.isSet()


if __name__ == "__main__":

    log_level = int(get_param_from_xml('LOG_LEVEL'))
    log_file_name = get_param_from_xml('LOG_FILE')

    logging.basicConfig(level=log_level, filename=log_file_name,
                        format='%(asctime)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

    if os.geteuid() != 0:
        logging.error("Failed to run because of permission...")
        exit("You need to have root privileges to run this script.\nPlease try again by using 'sudo'.\nExiting...")
    else:
        logging.info("Started...")

    debug = get_param_from_xml('DEBUG')

    dev_name_list = []

    print "Scanning Securimote devices..."
    print ""

    scanner = Scanner().withDelegate(ScanDelegate())
    devices = scanner.scan(int(get_param_from_xml('SCAN_DURATION')))

    allowed_list = get_dev_name_list()

    found_list = []
    for dev in devices:
        for (adtype, desc, value) in dev.getScanData():
            if desc == 'Complete Local Name':
                if value in allowed_list:
                    found_list.append((value, dev.addr, dev.rssi))
                    str_tmp = "Device found, name : %s, mac address : %s, strength=%d dB" % (value, dev.addr, dev.rssi)
                    print str_tmp
                    logging.info(str_tmp)

    for dev in found_list:
        print "Start new device..."
        # run thread (get data from device and upload to db)
        inst_wimoto = Securimote(dev)
        logging.info("New device(%s) is added." % dev[0])
        inst_wimoto.start()



